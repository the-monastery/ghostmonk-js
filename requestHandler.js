var exec = require( "child_process" ).exec;

function start( response ) {
	console.log( "Request handler 'Start' was called.");
	response.writeHead( 200, { "Content-Type": "text/plain" } );
	response.write( "Start" );
	response.end();	
}

function list( response ) {

	console.log( "Request handler 'List' was called." );

	exec( "ls", function( error, stdout, stderr ) {
		response.writeHead( 200, { "Content-Type": "text/plain" } );
		response.write( stdout );
		response.end();
	});

}

function find( response ) {

	console.log( "Request handler 'Find' was called." );

	exec( "find /", 
		{ timeout: 10000, maxBuffer: 20000 * 1024 },
		function( error, stdout, stderr ) {
			response.writeHead( 200, { "Content-Type": "text/plain" } );
			response.write( stdout );
			response.end();
		});

}

function upload( response ) {
	console.log( "Request handler 'upload' was called.");
	response.writeHead( 200, { "Content-Type": "text/plain" } );
	response.write( "Upload" );
	response.end();
}

exports.start = start;
exports.upload = upload;
exports.find = find;
exports.list = list;